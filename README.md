# minalloc - tiny memory allocator 

A experimental allocator for use in handling dynamic memory


## Building

```
$ gcc -DLOGGING=1 -I include/ src/main.c src/minalloc.c -o main && ./main
```

## Using

```
#include "minalloc.h"
#define HEAP_SIZE 1024
int main(int argc, char *argv[])
{
  uint8_t heap1[HEAP_SIZE];
  heap_t *h1 = minalloc_init(heap1, sizeof(heap1));

  minalloc_t *a1 = minalloc(32, h1);
  minalloc_free(a1, h1);

  return 0;
}
```
