#ifndef MINALLOC_H__
#define MINALLOC_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct allocation
{
  struct allocation *next;
  unsigned int refcounter;
  size_t size;
  uint8_t *heap;
} minalloc_t;

typedef struct heap
{
  const size_t size;
  // the current allocation
  minalloc_t *allocation;
  uint8_t memory[];
} heap_t;

/**
 * Initialize the heap at `buf` of `size`
 */
heap_t *minalloc_init(void *buf, size_t size);

/**
 * Attempt to allocate some data of size `requested` in heap.
 */
minalloc_t *minalloc(size_t requested, heap_t *heap);


/**
 * Resize the allocation to `newsize` bytes.
 *
 * Returns NULL if reallocation was not possible otherwise returns the new allocation.
 * Only supports size increase, not decrease.
 */
minalloc_t *minrealloc(minalloc_t *alloc, size_t newsize, heap_t *heap);

/**
 * Aggregate size of allocations where alloc->state == state
 */
size_t minalloc_mem_count(bool count_free, heap_t *heap);

/**
 * Free a item and compact
 */
minalloc_t *minalloc_free(minalloc_t *alloc, heap_t *heap);

#endif /* MINALLOC_H__ */