#include "minalloc.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#if LOGGING
#include <stdio.h>
#else
#undef DEBUG
#undef INFO
#undef WARN
#undef ERROR
#define DEBUG(...)
#define INFO(...)
#define WARN(...)
#define ERROR(...)
#endif

#if LOGGING && !defined(DEBUG)
#define DEBUG(fmt, ...) fprintf(stderr, "[DEBUG] " fmt, __VA_ARGS__)
#endif

#if LOGGING && !defined(INFO)
#define INFO(fmt, ...) fprintf(stderr, "[INFO]  " fmt, __VA_ARGS__)
#endif
#if LOGGING && !defined(WARN)
#define WARN(fmt, ...) fprintf(stderr, "[WARN]  " fmt, __VA_ARGS__)
#endif
#if LOGGING && !defined(ERROR)
#define ERROR(fmt, ...) fprintf(stderr, "[ERROR] " fmt, __VA_ARGS__)
#endif

heap_t *minalloc_init(void *buf, size_t size)
{
  heap_t *heap = (void *)buf;
  // the number of bytes available for us to use
  *(size_t *)&heap->size = size - sizeof(heap_t);

  INFO("Initializing heap %p; heap=%p; size=%zu; sizeof(minalloc_t)=%zu; sizeof(heap_t)=%zu\n", heap, heap->memory, heap->size, sizeof(minalloc_t), sizeof(heap_t));
  // initialize the heap
  memset(heap->memory, 0xff, heap->size);
  // make our first allocation, this is on continous free allocation
  // with the size set to heapsize - size of our own struct.
  heap->allocation = buf + size - sizeof(minalloc_t);
  heap->allocation->heap = heap->memory;
  heap->allocation->refcounter = 0;
  heap->allocation->size = heap->size - sizeof(minalloc_t);
  heap->allocation->next = NULL;

  return heap;
}

size_t minalloc_mem_count(bool count_free, heap_t *heap)
{
  size_t size = 0;
  for (minalloc_t *alloc = heap->allocation; NULL != alloc; alloc = alloc->next)
  {
    if ((count_free && alloc->refcounter == 0) || (!count_free && alloc->refcounter > 0))
    {
      size += alloc->size;
    }
  }
  return size;
}

minalloc_t *minalloc(size_t requested, heap_t *heap)
{
  minalloc_t *alloc = NULL;
  /** find a free allocation if possible */
  for (alloc = heap->allocation->next; alloc != NULL && 0 != alloc->refcounter; alloc = alloc->next)
  {
  }

  /** if we have an allocation and free space available */
  if (NULL != alloc && heap->allocation->size >= requested)
  {
    alloc->refcounter = 1;
    alloc->size = requested;
    alloc->heap = heap->allocation->heap;

    heap->allocation->size = heap->allocation->size - requested;
    INFO("Allocated %p @ %ld - %zu bytes; remaining %zu\n", alloc, alloc->heap - heap->memory, alloc->size, heap->allocation->size);
    return alloc;
  }
  else if (heap->allocation->size >= (requested + sizeof(*alloc)))
  {
    alloc = heap->allocation;
    heap->allocation = (minalloc_t *)((uint8_t *)alloc - sizeof(*alloc));
    heap->allocation->refcounter = 0;
    // ensure that we consider the additional space taken by the allocations when
    // updating the amount of free space available.
    heap->allocation->size = alloc->size - requested - sizeof(*alloc);
    heap->allocation->next = alloc;
    heap->allocation->heap = alloc->heap + requested;

    alloc->refcounter = 1;
    alloc->size = requested;

    INFO("Allocated %p -> %p - %zu bytes; next=%p, remaining %zu\n", alloc, alloc->heap, alloc->size, heap->allocation->next, heap->allocation->size);
    return alloc;
  }
  else
  {
    // not enough free space
    WARN("Failed to allocate %zu+%ld bytes, only %zu remaining\n", requested, NULL == alloc ? sizeof(*alloc) : 0, heap->allocation->size);
    return NULL;
  }
}

minalloc_t *minrealloc(minalloc_t *alloc, size_t requested, heap_t *heap)
{
  const size_t increase = requested - alloc->size;
  const size_t contigous_free_space_after_realloc = heap->allocation->size - increase;

  /* only increase is allowed*/
  if (alloc->size > requested)
  {
    WARN("Can't decrease size of allocation, was %zu, requested %zu\n", alloc->size, requested);
    return NULL;
  }

  /** ensure there's enough space */
  if (increase > heap->allocation->size)
  {
    WARN("Can't increase size of allocation with %zu bytes, was %zu, requested %zu\n", increase, alloc->size, requested);
    return NULL;
  }

  // how many bytes are used after (alloc->heap + alloc->size)?
  INFO("Moving (%p) %zu bytes from p@%ld to p@%ld\n", heap->allocation,
       contigous_free_space_after_realloc,
       (alloc->heap + alloc->size) - heap->memory,
       (alloc->heap + alloc->size + increase) - heap->memory);

  // move everything after this allocation by `requested`
  memmove(alloc->heap + alloc->size + increase, alloc->heap + alloc->size,
          contigous_free_space_after_realloc);

  for (minalloc_t *a = heap->allocation; NULL != a; a = a->next) {
    // if the heap is below us in memory there's no need to move
    if (a->heap <= alloc->heap) {
      continue;
    }

    // shift the pointer to new position
    DEBUG("Shifting heap from @%ld to @%ld\n", a->heap - heap->memory, (a->heap + increase) - heap->memory);
    a->heap += increase;
  }

  heap->allocation->size -= increase;
  alloc->size += increase;

  return alloc;
}

static void compact(minalloc_t *alloc, heap_t *heap)
{
  uint8_t *target = alloc->heap;
  uint8_t *source = alloc->heap + alloc->size;
  uint8_t *tmp = NULL;
  int8_t moved = -1;

  /* move all memory segments after `alloc` to the left. allocations are
   * not in order such that all allocations must be checked. Once we have
   * an heap segment not moved its due to a lacking allocation and we have
   * moved everything.
   *
   * The allocation could be ordered stored in order so we don't need to
   * reiterate over for every allocation but that would increase ram usage
   * for some work loads which is not an otion.
   */
  while (moved != 0)
  {
    moved = 0;
    DEBUG("Looking to move %ld -> %ld\n", source - heap->memory, target - heap->memory);
    for (minalloc_t *a = heap->allocation; NULL != a; a = a->next)
    {
      if (source == a->heap)
      {
        DEBUG("MOVE %zu bytes from %zu -> %ld\n", a->size, a->heap - heap->memory, target - heap->memory);
        memcpy(target, a->heap, a->size);

        tmp = target;
        target = a->heap;
        source = target + a->size;
        a->heap = tmp;

        moved = 1;
        break;
      }
    }
  }

  heap->allocation->size += alloc->size;
  alloc->size = 0;

  while (heap->allocation->next)
  {
    /* if the next allocation in the list is not free we can't compact */
    if (heap->allocation->next->refcounter > 0)
    {
      break;
    }

    DEBUG("Compact allocation %p of size %zu with %p of size %zu to total of %zu bytes\n", heap->allocation, heap->allocation->size, heap->allocation->next, heap->allocation->next->size, heap->allocation->size + heap->allocation->next->size);

    heap->allocation->next->size += heap->allocation->size + sizeof(*heap->allocation);
    heap->allocation->size = 0;
    heap->allocation->heap = NULL;
    heap->allocation = heap->allocation->next;
  }
}

minalloc_t *minalloc_free(minalloc_t *alloc, heap_t *heap)
{
  if (0 == --alloc->refcounter)
  {
    INFO("Freeing %ld of size %zu (alloc=%p)\n", alloc->heap - heap->memory, alloc->size, alloc);
    compact(alloc, heap);
    return NULL;
  }

  return alloc;
}
