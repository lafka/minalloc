#include "minalloc.h"

#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#define HEAP_SIZE 1024

void simple_test(void)
{
  uint8_t heap1[HEAP_SIZE];
  heap_t *h1 = minalloc_init(heap1, sizeof(heap1));
  minalloc_t *a1 = minalloc(32, h1);
  minalloc_t *a2 = minalloc(32, h1);
  minalloc_t *a3 = minalloc(32, h1);
  minalloc_free(a3, h1);
  minalloc_free(a2, h1);
  minalloc_free(a1, h1);
}

bool all(uint8_t *buf, size_t size, char c)
{
  for (int i = 0; i < size; i++)
  {
    if (c != buf[i])
    {
      return false;
    }
  }

  return true;
}

void defragment_test(void)
{
  // space for one allocation and 5 slots of 128mb/
  uint8_t storage[sizeof(heap_t) + ((sizeof(minalloc_t) + 128) * 5)];
  heap_t *heap = minalloc_init(storage, sizeof(storage));

  minalloc_t *a1 = minalloc(128, heap);
  minalloc_t *a2 = minalloc(128, heap);
  minalloc_t *a3 = minalloc(128, heap);
  minalloc_t *a4 = minalloc(128, heap);

  memset(a1->heap, '1', 128);
  memset(a2->heap, '2', 128);
  memset(a3->heap, '3', 128);
  memset(a4->heap, '4', 128);

  assert(512 == minalloc_mem_count(false, heap));
  assert(128 == minalloc_mem_count(true, heap));

  assert(all(a1->heap, a1->size, '1'));
  assert(all(a2->heap, a2->size, '2'));
  assert(all(a3->heap, a3->size, '3'));
  assert(all(a4->heap, a4->size, '4'));

  // free first and last one. After this a2->heap should point to the a1->heap
  // and a4->heap should point to a2->heap. The allocations are not compacted.
  uint8_t *old_a1 = a1->heap;
  uint8_t *old_a2 = a2->heap;
  uint8_t *old_a3 = a3->heap;
  // uint8_t *old_a4 = a4->heap;

  minalloc_free(a1, heap);

  assert(all(old_a1, 128, '2'));
  assert(all(old_a2, 128, '3'));
  assert(all(old_a3, 128, '4'));

  minalloc_free(a3, heap);
  assert(all(old_a1, 128, '2'));
  assert(all(old_a2, 128, '4'));

  assert(256 == minalloc_mem_count(false, heap));
  assert(384 == minalloc_mem_count(true, heap));

  // this re-uses the a1 allocation but a1->heap now points to what used to be a3->heap
  assert(NULL != (a1 = minalloc((128 * 3), heap)));

  assert(640 == minalloc_mem_count(false, heap));
  assert(0 == minalloc_mem_count(true, heap));

  memset(a1->heap, 'x', a1->size);

  assert(all(old_a1, 128, '2'));
  assert(all(a2->heap, 128, '2'));
  assert(all(old_a2, 128, '4'));
  assert(all(a4->heap, 128, '4'));

  // allocating one byte should fail since we are out of space
  assert(NULL == minalloc(1, heap));
  // after this free only a1, a2 is used and some free space at the end.
  minalloc_free(a4, heap);
}

void refcounter_test(void) {
    // space for one allocation and 5 slots of 128mb/
  uint8_t storage[sizeof(heap_t) + ((sizeof(minalloc_t) + 128) * 5)];
  heap_t *heap = minalloc_init(storage, sizeof(storage));

  minalloc_t *a1 = minalloc(128, heap);


  a1->refcounter++;

  assert(2 == a1->refcounter);
  minalloc_free(a1, heap);
  assert(1 == a1->refcounter);
  minalloc_free(a1, heap);
  assert(0 == a1->refcounter);

  minalloc_t *a2 = minalloc(128, heap);

  // since we've released a1 heap should now point to same place
  // and the allocation should be reused
  assert(a1 == a2);
  assert(a1->heap == a2->heap);
}

void realloc_test(void) {
  uint8_t storage[sizeof(heap_t) + ((sizeof(minalloc_t) + 32) * 5)];

  heap_t *heap = minalloc_init(storage, sizeof(storage));
  minalloc_t *a1 = minalloc(32, heap);
  minalloc_t *a2 = minalloc(32, heap);
  minalloc_t *a3 = minalloc(32, heap);

  memset(a1->heap, '1', a1->size);
  memset(a2->heap, '2', a2->size);
  memset(a3->heap, '3', a3->size);

  minrealloc(a1, 64, heap);

  assert(64 == a1->size);
  memset(a1->heap, '1', a1->size);
  assert(32 == a2->size);
  assert(32 == a3->size);
  assert(all(a1->heap, a1->size, '1'));
  assert(all(a2->heap, a2->size, '2'));
  assert(all(a3->heap, a3->size, '3'));

}

int main(int argc, char *argv[])
{
  simple_test();
  defragment_test();
  refcounter_test();
  realloc_test();

  return 0;
}
